/*1. Переменную в JS можно объявить с помощью let,const,var при использовании "use strict". Так же переменную можно обявить без let,const и var, но это плохая практика и не отработает в "use strict".
    2. Фунцкия promt() запрашивает у пользователя ответ напечатать, а фунцкия confirm() дает только право выбора "ок" или "отмена", используя Boolean.
        3. Неявное преобразование - это конвертанция разных типов данных, которое происходит автоматически. */

"use strict"
let nameUser = 'Denys';
let admin;
admin = nameUser;
console.log(admin);

let days = 9;
console.log(days*((24*60)*60));

let age = prompt ('Введите ваш возраст');
console.log(age);
